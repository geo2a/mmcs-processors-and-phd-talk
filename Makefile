all: slides.html

slides.html: preamble.html proc-specs.html uk-phd.html epilogue.html
	cat preamble.html proc-specs.html uk-phd.html epilogue.html > slides.html

clean:
	rm slides.html